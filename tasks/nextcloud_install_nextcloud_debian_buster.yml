---

- name: NextCloud Install {{ansible_distribution}} {{ansible_distribution_release}} | Abfrage Installation vorhanden
  stat: 
    path: "{{www_install_path}}"
  register: result_packet

- name: NextCloud Install {{ansible_distribution}} {{ansible_distribution_release}} | Abfrage Grundkonfig vorhanden
  stat:
    path: "{{www_install_path}}/config/config.php"
  register: result_config

- name: NextCloud Install {{ansible_distribution}} {{ansible_distribution_release}} | Abfrage ApacheConfig vorhanden
  stat:
    path: "/etc/apache2/sites-enabled/nextcloud-https.conf"
  register: result_apache

- name: NextCloud Install {{ansible_distribution}} {{ansible_distribution_release}} | Generiertes Passwort DB auslesen
  set_fact: 
    occ_db_pass: "{{ occ_db_pwd }}"

- name: NextCloud Install {{ansible_distribution}} {{ansible_distribution_release}} | Generiertes Passwort Admin auslesen
  set_fact: 
    occ_admin_pass: "{{ occ_admin_pwd }}"

- name: NextCloud Install {{ansible_distribution}} {{ansible_distribution_release}} | NextCloud deaktivieren
  command: 'a2dissite nextcloud-https.conf'
  when: result_apache.stat.exists == True

- name: NextCloud Install {{ansible_distribution}} {{ansible_distribution_release}} | Apache Reload wenn NextCloud Config vorhanden
  service:
    name: apache2
    state: restarted
  when: result_apache.stat.exists == True

- name: NextCloud Install {{ansible_distribution}} {{ansible_distribution_release}} | Abhängigkeiten installieren
  package:
    name: "{{ item }}"
    state: present
  with_items:  
    - php-imagick
    - php7.3-curl
    - php7.3-dom
    - php7.3-iconv
    - php7.3-json
    - php7.3-mbstring
    - php7.3-posix
    - php7.3-mysql 
    - php7.3-simplexml
    - php7.3-zip 
    - php7.3-pdo-mysql
    - php7.3-bz2 
    - php7.3-intl
    - php7.3-ldap
    - php7.3-smbclient
    - php7.3-apcu
    - php7.3-redis
    - php7.3-gd
    - php7.3-xml
    - php7.3-zip
    - php7.3-intl
    - php7.3-bz2 
    - php7.3-ldap
    - php7.3-bcmath
    - php7.3-gmp
    - php7.3-igbinary
    - php7.3-cli
    - php7.3-readline
    - php7.3-opcache
    - redis-server
    - sudo
    - unzip
    - php-bcmath 
    - php-gmp 
    - imagemagick 
    - libmagickcore-6.q16-6-extra
    - ffmpeg
    - php-smbclient
    - samba-common

- name: NextCloud Install {{ansible_distribution}} {{ansible_distribution_release}} | Download NextCloud Package
  get_url:
    url: https://download.nextcloud.com/server/releases/{{ nc_download_file }}
    dest: /root/{{ nc_download_file }}
  when: result_packet.stat.exists == False

- name: NextCloud Install {{ansible_distribution}} {{ansible_distribution_release}} | NextCloud Package entpacken
  unarchive:
    src: /root/{{ nc_download_file }}
    dest: /var/www/html/
    remote_src: yes
  when: result_packet.stat.exists == False

- name: NextCloud Install {{ansible_distribution}} {{ansible_distribution_release}} | Download Paket löschen
  file:
    path: /root/{{ nc_download_file }}
    state: absent

- name: NextCloud Install {{ansible_distribution}} {{ansible_distribution_release}} | Anpassen Rechte Webordner "{{ www_install_path }}"
  file: 
    path: "{{ www_install_path }}"
    recurse: yes
    state: directory
    owner: www-data
    group: www-data

- name: NextCloud Install {{ansible_distribution}} {{ansible_distribution_release}} | Ordner für OpCache anlegen
  file:
    path: /var/www/opcache
    state: directory
    mode: 0755
    owner: www-data
    group: www-data
  
- name: NextCloud Install {{ansible_distribution}} {{ansible_distribution_release}} | OpCache konfigurieren 
  copy:
    src: "{{ role_path }}/files/99-afi-opcache.ini"
    dest: /etc/php/7.3/apache2/conf.d

- name: NextCloud Install {{ansible_distribution}} {{ansible_distribution_release}} | PHP Memory Limit anpassen
  copy:
    src: "{{ role_path }}/files/99-afi-nextcloud.ini"
    dest: /etc/php/7.3/apache2/conf.d

- name: NextCloud Install {{ansible_distribution}} {{ansible_distribution_release}} | Skeleton Ordner anpassen
  file:
    path: "{{ www_install_path }}/core/skeleton/Photos"
    state: absent

- name: NextCloud Install {{ansible_distribution}} {{ansible_distribution_release}} | Skeleton Ordner anpassen
  file:
    path: "{{ www_install_path }}/core/skeleton/Documents"
    state: absent

- name: NextCloud Install {{ansible_distribution}} {{ansible_distribution_release}} | Skeleton Ordner anpassen
  file:
    path: "{{ www_install_path }}/core/skeleton/Templates"
    state: absent

- name: NextCloud Install {{ansible_distribution}} {{ansible_distribution_release}} | Skeleton Ordner anpassen
  file:
    path: "{{ www_install_path }}/core/skeleton/{{ item }}"
    state: absent
  with_items:
    - Reasons to use Nextcloud.pdf
    - Nextcloud Manual.pdf
    - Nextcloud intro.mp4

- name: NextCloud Install {{ansible_distribution}} {{ansible_distribution_release}} | Skeleton Ordner anpassen
  file:
    path: "{{ www_install_path }}/core/skeleton/Nextcloud.png"
    state: absent

- name: NextCloud Install {{ansible_distribution}} {{ansible_distribution_release}} | Skeleton Ordner anpassen
  file:
    path: "{{ www_install_path }}/core/skeleton/Nextcloud.mp4"
    state: absent


- name: NextCloud Install {{ansible_distribution}} {{ansible_distribution_release}} | erstelle NextCloud DB
  community.mysql.mysql_db:
    name: "nextcloud"
    state: present

- name: NextCloud Install {{ansible_distribution}} {{ansible_distribution_release}} | erstelle NextCloud DB User
  community.mysql.mysql_user:
    name: nextcloud
    password: '{{ occ_db_pass }}'
    priv: " nextcloud.*:ALL"
    state: present

- name: NextCloud Install {{ansible_distribution}} {{ansible_distribution_release}} | erstellen OwnCloud Data Dir
  file:
    path: "{{www_data_path}}"
    state: directory
    owner: www-data
    group: www-data

- name: NextCloud Install {{ansible_distribution}} {{ansible_distribution_release}} | Grund Config NextCloud erstellen und Datenbank initialisieren
  command: php occ maintenance:install --database "mysql" --database-name "nextcloud" --database-host "localhost" --database-user "nextcloud" --database-pass "{{ occ_db_pass }}" --admin-user "admin" --admin-pass "{{ occ_admin_pass }}" --data-dir "{{www_data_path}}"
  become: yes
  become_user: www-data
  args:
    chdir: "{{ www_install_path }}"
  when: result_config.stat.exists == False

- name: NextCloud Install {{ansible_distribution}} {{ansible_distribution_release}} | Erweitere Konfig JSON File erzeugen
  template: 
    src: templates/nextcloud.json.j2 
    dest: "{{ www_install_path }}/nextcloud.json"
  #when: result_config.stat.exists == False

- name: NextCloud Install {{ansible_distribution}} {{ansible_distribution_release}} | Erweitere Konfig OwnCloud erstellen (Redis/AcPU/FileLocking)
  command: php occ config:import nextcloud.json
  become: yes
  become_user: www-data
  args:
    chdir: "{{ www_install_path }}"
  #when: result_config.stat.exists == False

#- name: NextCloud Install {{ansible_distribution}} {{ansible_distribution_release}} | Erweitere Konfig OwnCloud erstellen (Redis/AcPU/FileLocking)
#  command:
#  become: yes
#  become_user: www-data
#  args:
#    chdir: "{{ www_install_path }}"
#  with_items:
#    - php occ config:system:set memcache.local --value \\OC\\Memcache\\Redis
#    - php occ config:system:set memcache.distributed --value \\OC\\Memcache\\Redis
#    - php occ config:system:set memcache.locking --value \\OC\\Memcache\\Redis
#    - php occ config:system:set mail_domain --value {{occ_domain.split('.').1}}.{{occ_domain.split('.').2}}
#    - php occ config:system:set mail_from_address --value nextcloud
#    - php occ config:system:set mail_smtpmode --value smtp
#    - php occ config:system:set overwrite.cli.url --value https://{{ occ_domain }}
#    - php occ config:system:set defaultapp --value files
#    - php occ config:system:set loglevel --value 2
#    - php occ config:system:set enable_previews --value true
#    - php occ config:system:set smb.logging.enable --value false
#    - php occ config:system:set dav.enable.async --value false
#    - php occ config:system:set default_phone_region --value DE
#    - php occ config:system:set default_language --value de
#    - php occ config:system:set default_locale --value de_DE
#    - php occ config:system:set htaccess.RewriteBase --value /
#    - php occ config:system:set trusted_proxies 1 --value {{ ansible_default_ipv4.address }}
#    - php occ config:system:set trusted_proxies 2 --value {{ occ_proxy_ip }}
#    - php occ config:system:set trusted_domains 1 --value localhost
#    - php occ config:system:set trusted_domains 2 --value {{ occ_domain }}
#    - php occ config:system:set trusted_domains 3 --value {{ ansible_default_ipv4.address }}

- name: NextCloud Install {{ansible_distribution}} {{ansible_distribution_release}} | Erweitere Konfig JSON File löschen
  file:
    path: "{{ www_install_path }}/nextcloud.json"
    state: absent

- name: NextCloud Install {{ansible_distribution}} {{ansible_distribution_release}} | Weitere Apps aktivieren
  command: php occ app:enable "{{ item }}"
  with_items:
    - files_external
  become: yes
  become_user: www-data
  args:
    chdir: "{{ www_install_path }}"

- name: NextCloud Install {{ansible_distribution}} {{ansible_distribution_release}} | Anpassungen NextCloud Datenbank db:add-missing-indices
  command: php occ db:add-missing-indices
  become: yes
  become_user: www-data
  args:
    chdir: "{{ www_install_path }}"

- name: NextCloud Install {{ansible_distribution}} {{ansible_distribution_release}} | Anpassungen NextCloud Datenbank BigInit DB Erweiterung
  command: php occ db:convert-filecache-bigint -n
  become: yes
  become_user: www-data
  args:
    chdir: "{{ www_install_path }}"

- name: NextCloud Install {{ansible_distribution}} {{ansible_distribution_release}} | Anpassungen NextCloud keine Anzeige index.php
  command: php occ maintenance:update:htaccess
  become: yes
  become_user: www-data
  args:
    chdir: "{{ www_install_path }}"

- name: NextCloud Install {{ansible_distribution}} {{ansible_distribution_release}} | CronJob NextCloud cron.php erstellen 
  cron:
    name: 'NextCloud cron.php every 5 mins'
    job: 'php -f /var/www/html/nextcloud/cron.php'
    user: www-data
    weekday: '*'
    minute: '*/5'
    hour: '*'
    month: '*'

- name: NextCloud Install {{ansible_distribution}} {{ansible_distribution_release}} | CronJob Trashbin erstellen
  cron:
    name: 'NextCloud TrashBin:cleanup'
    job: 'php -f /var/www/html/nextcloud/occ trashbin:cleanup --all-users -n'
    user: www-data
    weekday: '*'
    minute: '4'
    hour: '23'
    month: '*'

- name: NextCloud Install {{ansible_distribution}} {{ansible_distribution_release}} | NextCloud Aliases erzeugen
  copy:
    src: "{{ role_path }}/files/99-afi-nc-aliases.sh"
    dest: /etc/profile.d/